<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-header">Últimas Receitas</h1>  
            </div>
            <?php foreach ($receitas as $row): ?>
                <div class="col-sm-6" >
                    <div class="caixa-receita">
                        <div class="row">
                            <a href="<?= base_url('usu/usuario/receitas/' . $row->slug_receita) ?>">
                                <div class="col-sm-12">
                                    <img class="img-responsive" src="<?= base_url('imagens/receitas/' . $row->foto) ?>"/>
                                </div>
                                <div class="col-sm-12">
                                    <h4 class="nome"><?= $row->receita; ?></h4>
                                    <h5><?= $row->nome; ?></h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <a class="btn btn-default" href="<?= base_url('receitas') ?>">Mais receitas...</a>
    </div>   
</div>



<?php foreach ($produtos as $row): ?>
    <div class="col col-md-4">
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Free</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>10 users included</li>
                        <li>2 GB of storage</li>
                        <li>Email support</li>
                        <li>Help center access</li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary">Comprar</button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>