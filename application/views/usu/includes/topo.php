<!DOCTYPE html>
<html lang="en">
    <head>
        <title>loja Virtual</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/main.css" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/dashboard.css" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/pricing.css" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/carousel.css" rel="stylesheet">
        <link href="<?= base_url(); ?>dist/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="icon" href="<?= base_url(); ?>dist/img/favicon.ico">


        <script type="text/javascript" src="<?= base_url(); ?>dist/js/loja.js" ></script>

        <!-- jQuery CDN -->
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-45459840-1']);
            _gaq.push(['_trackPageview', '']);
            _gaq.push(['Tray._setAccount', 'UA-6914032-20']);
            _gaq.push(['Tray._setAllowLinker', true]);
            _gaq.push(['Tray._trackPageview', '/home/index?only_featured=1&order=rand']);
            _gaq.push(['Tray._setCustomVar', 1, 'storeId', '352023']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <!-- Inicio bloco header --><script>dataLayer = [{"pageTitle": "Loja Oficial DC Comics", "pageCategory": "Home", "event": "", "siteSearchFrom": "https:\/\/www.google.com.br\/"}]</script>
        <script>
            var gtmTray = document.createElement("script");
            gtmTray.src = "/mvc/store/352023/google_tag_manager/updateGTM.js?" + Date.now();
            document.querySelector("head").appendChild(gtmTray);
        </script>

        <script>
            Holder.addTheme('thumb', {
                bg: '#55595c',
                fg: '#eceeef',
                text: 'Thumbnail'
            });
        </script>

        <!-- Fim bloco header -->


        <!-- theme-options -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header class="header">
            <div class="container">                
                <nav class="navbar-nav pull-left ">
                    <h2 class="hide">Serviços</h2>
                    <ul class="nav navbar-nav">
                        <li><a href="<?= base_url('produtos') ?>">Produtos</a></li>
                        <li><a href="<?= base_url('contato') ?>">Contato</a></li>                        
                        <li><a href="<?= base_url('admin') ?>">Alto Contraste</a></li>
                        <li><a href="<?= base_url('/acessibilidade') ?>">Acessibilidade</a></li>
                        <li><a href="<?= base_url('admin') ?>">Mapa do site</a></li>
                        <li><a href="<?= base_url('admin') ?>">Acessar</a></li>
                    </ul>
                    <span class="hide">Fim do menu de serviços</span>
                </nav>
            </div>


            <!-- Inicio bloco top -->
            <!-- Google Tag Manager -->
            <noscript><iframe src='//www.googletagmanager.com/ns.html?id=GTM-W9F2C5'
                              height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-W9F2C5');</script>
            <!-- End Google Tag Manager --> 
            <!-- Fim bloco top -->


            <div class="menu-mobile-backdrop visible-xs-block"></div>
            <div class="menu-mobile visible-xs-block">
                <div class="btnVoltarNivel1">
                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                    Voltar
                </div>
                <ul class="categories-mobile level1">
                    <li class="item-level1">
                        <input type="checkbox" id="mobile-item-478" class="level1-check"/>
                        <label class="link-level1" for="mobile-item-478" data-tray-tst="categoria_lvl_1">
                            <i class="pull-right arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                            Personagens
                        </label>

                        <div class="level2">
                            <div class="visible-xs-block">
                                <div class="btnVoltarNivel2">
                                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                                    Voltar
                                </div>
                                <h5>Personagens</h5>
                                <ul class="categories-mobile level1">
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/personagens/comics" class="link-level2" data-tray-tst="categoria_lvl_2">Comics</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/personagens/series-filmes-games" class="link-level2" data-tray-tst="categoria_lvl_2">Séries / Filmes / Games</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="item-level1">
                        <input type="checkbox" id="mobile-item-486" class="level1-check"/>
                        <label class="link-level1" for="mobile-item-486" data-tray-tst="categoria_lvl_1">
                            <i class="pull-right arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                            Moda
                        </label>

                        <div class="level2">
                            <div class="visible-xs-block">
                                <div class="btnVoltarNivel2">
                                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                                    Voltar
                                </div>
                                <h5>Moda</h5>
                                <ul class="categories-mobile level1">
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/camisetas-masculinas" class="link-level2" data-tray-tst="categoria_lvl_2">Roupas Masculinas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/camisetas-femininas" class="link-level2" data-tray-tst="categoria_lvl_2">Roupas Femininas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/infantil" class="link-level2" data-tray-tst="categoria_lvl_2">Roupas Infantis</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/acessorios" class="link-level2" data-tray-tst="categoria_lvl_2">Acessórios</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/calcados" class="link-level2" data-tray-tst="categoria_lvl_2">Calçados</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/colecao-de-inverno" class="link-level2" data-tray-tst="categoria_lvl_2">Coleção de Inverno</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/moda/combos" class="link-level2" data-tray-tst="categoria_lvl_2">Combos</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/patins" class="link-level2" data-tray-tst="categoria_lvl_2">Patins</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="item-level1">
                        <input type="checkbox" id="mobile-item-30" class="level1-check"/>
                        <label class="link-level1" for="mobile-item-30" data-tray-tst="categoria_lvl_1">
                            <i class="pull-right arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                            Casa e Decoração
                        </label>

                        <div class="level2">
                            <div class="visible-xs-block">
                                <div class="btnVoltarNivel2">
                                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                                    Voltar
                                </div>
                                <h5>Casa e Decoração</h5>
                                <ul class="categories-mobile level1">
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/almofadas" class="link-level2" data-tray-tst="categoria_lvl_2">Almofadas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/moveis" class="link-level2" data-tray-tst="categoria_lvl_2">Móveis</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/organizadores" class="link-level2" data-tray-tst="categoria_lvl_2">Organizadores</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/canecas-e-copos" class="link-level2" data-tray-tst="categoria_lvl_2">Canecas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/placas-decorativas" class="link-level2" data-tray-tst="categoria_lvl_2">Placas Decorativas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/bonecos-colecionaveis" class="link-level2" data-tray-tst="categoria_lvl_2">Colecionáveis</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/quadros-e-posteres" class="link-level2" data-tray-tst="categoria_lvl_2">Quadros</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/copos" class="link-level2" data-tray-tst="categoria_lvl_2">Copos</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/radio-relogios-despertadores" class="link-level2" data-tray-tst="categoria_lvl_2">Relógios e Despertadores</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/cozinha" class="link-level2" data-tray-tst="categoria_lvl_2">Cozinha</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/squeezes" class="link-level2" data-tray-tst="categoria_lvl_2">Squeezes e Garrafas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/quebra-cabecas" class="link-level2" data-tray-tst="categoria_lvl_2">Quebra-Cabeças</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/casa-e-decoracao/luminarias" class="link-level2" data-tray-tst="categoria_lvl_2">Luminárias</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="item-level1">
                        <input type="checkbox" id="mobile-item-494" class="level1-check"/>
                        <label class="link-level1" for="mobile-item-494" data-tray-tst="categoria_lvl_1">
                            <i class="pull-right arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                            Acessórios
                        </label>

                        <div class="level2">
                            <div class="visible-xs-block">
                                <div class="btnVoltarNivel2">
                                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                                    Voltar
                                </div>
                                <h5>Acessórios</h5>
                                <ul class="categories-mobile level1">
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/audio-e-gadgets" class="link-level2" data-tray-tst="categoria_lvl_2">Áudio e Gadgets</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/button" class="link-level2" data-tray-tst="categoria_lvl_2">Buttons</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/informatica-e-acessorios" class="link-level2" data-tray-tst="categoria_lvl_2">Informática e Acessórios</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/uso-pessoal" class="link-level2" data-tray-tst="categoria_lvl_2">Uso Pessoal</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/chaveiros" class="link-level2" data-tray-tst="categoria_lvl_2">Chaveiros</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/patins" class="link-level2" data-tray-tst="categoria_lvl_2">Patins</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/prancha-de-surfe" class="link-level2" data-tray-tst="categoria_lvl_2">Prancha de Surfe</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/skates" class="link-level2" data-tray-tst="categoria_lvl_2">Skates</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/acessorios/capacetes" class="link-level2" data-tray-tst="categoria_lvl_2">Capacetes</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/cosmeticos" class="link-level2" data-tray-tst="categoria_lvl_2">Cosméticos</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="item-level1">
                        <input type="checkbox" id="mobile-item-496" class="level1-check"/>
                        <label class="link-level1" for="mobile-item-496" data-tray-tst="categoria_lvl_1">
                            <i class="pull-right arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                            Filmes / TVs / Games
                        </label>

                        <div class="level2">
                            <div class="visible-xs-block">
                                <div class="btnVoltarNivel2">
                                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                                    Voltar
                                </div>
                                <h5>Filmes / TVs / Games</h5>
                                <ul class="categories-mobile level1">
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/dvds-e-blu-ray" class="link-level2" data-tray-tst="categoria_lvl_2">DVDs e Blu-Ray</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/games" class="link-level2" data-tray-tst="categoria_lvl_2">Games</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="item-level1">
                        <input type="checkbox" id="mobile-item-498" class="level1-check"/>
                        <label class="link-level1" for="mobile-item-498" data-tray-tst="categoria_lvl_1">
                            <i class="pull-right arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                            Livraria / Papelaria
                        </label>

                        <div class="level2">
                            <div class="visible-xs-block">
                                <div class="btnVoltarNivel2">
                                    <i class="arrow-menu-mobile"><img src="https://static3.tcdn.com.br/352023/themes/169/img/arrow-right.svg?8de69d4f15528b26f1af715c0e8f4435" height="16" width="16" alt="Seta"></i>
                                    Voltar
                                </div>
                                <h5>Livraria / Papelaria</h5>
                                <ul class="categories-mobile level1">
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/livraria-papelaria/mochilas" class="link-level2" data-tray-tst="categoria_lvl_2">Mochilas</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/papelaria" class="link-level2" data-tray-tst="categoria_lvl_2">Cadernos</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/produtos/graphic-novel-e-hq" class="link-level2" data-tray-tst="categoria_lvl_2">Graphic Novel e HQ</a>
                                    </li>
                                    <li class="item-level2">
                                        <a href="https://www.lojadccomics.com.br/livraria-papelaria/blocos-de-anotacoes" class="link-level2" data-tray-tst="categoria_lvl_2">Blocos de Anotações</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="item-level1">
                        <a href="https://www.lojadccomics.com.br/novidades" class="link-level1" data-tray-tst="categoria_lvl_1">Fan Box</a>

                    </li>
                </ul>
            </div> 




            <div class="search-row">
                <div class="container">
                    <div class="col-sm-2 col-xs-12">
                        <a href="/"><img class="img-circle imagem-logo"src="<?= base_url() ?>dist/img/lojavirtual.jpg" alt="imagem de loja virtual"/></a>
                    </div>

                    <div class="mm-menuitems-area col-sm-7 col-xs-12">
                        <form method="get" action="" class="search" name="buscaForm" data-search="suggestion">
                            <input name="loja" value="352023" type="hidden">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="input-group mm-input-group">
                                        <input type="text" value="pesquise" title="pesquisar na loja" class="form-control search-key" data-input="suggestion" placeholder="Procurar na loja" tabindex="0" name="palavra_busca" autocomplete="off" >
                                        <span class="input-group-btn">
                                            <button class="btn btn-default glyphicon glyphicon-search search-button" type="submit"></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="mm-menuitems-area col-sm-3 col-xs-12 customer-menu">
                        <ul class="tray-hide" data-logged-user="true">
                            <li>
                                <a href="/central-do-cliente">
                                    <div class="mm-area-icons">
                                        <img alt="central do cliente" src="<?= base_url() ?>dist/img/user_icon.jpg">
                                        <span>Central</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/loja/carrinho.php?loja=352023" class="cart">
                                    <div class="mm-area-icons">
                                        <img alt="carrinho de compras" src="<?= base_url() ?>dist/img/carrinho.jpg">
                                        <span data-cart="amount" class="amount">0</span>
                                        <span>Carrinho</span>
                                    </div>
                                    <div class="list">
                                        <div>
                                            <ul></ul>
                                            <div class="subtotal">
                                                <span data-cart="price">0,00</span>
                                                Subtotal
                                                <button>Finalizar Compra</button>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </li>
                        </ul>
                        <ul class="" data-logged-user="false">
                            <li class="login">
                                <a href="/central-do-cliente">
                                    <div class="mm-area-icons">
                                        <img alt="login de usuário" src="<?= base_url() ?>dist/img/login.png">
                                        <span>LOGIN</span>
                                    </div>
                                </a>
                                <div class="login-box">
                                    <form action="" method="post" name="form_login">
                                        <h3>Acessar conta</h3>
                                        <label for="email_login">email
                                            <input name="email_login" id="email_login" placeholder="Seu e-mail" required="" type="email"></label>
                                        <label for="senha_login">senha
                                            <input name="senha_login" id="senha_login" placeholder="Sua senha" required="" type="password"></label>

                                        <button type="submit" name="login">LOGIN</button>
                                        <a href="/cadastro">Cadastre-se</a>
                                    </form>
                                </div>
                            </li>
                            <li>
                                <a href="/loja/carrinho.php?loja=352023" class="cart">
                                    <div class="mm-area-icons">
                                        <img alt="carrinho de compras" src="<?= base_url() ?>dist/img/carrinho.png">
                                        <span data-cart="amount" class="amount">0</span>
                                        <span>Carrinho</span>
                                    </div>
                                    <div class="list">
                                        <div>
                                            <ul></ul>
                                            <div class="subtotal">
                                                <span data-cart="price">0,00</span>
                                                Subtotal
                                                <button>Finalizar Compra</button>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- category-list -->
            <span class="show-categories">Navegar por categorias</span>
            <nav class="main-menu" role="navigation">
                <div class="container">
                    <ul class="level1">
                        <li class="item-level1 link-novidades">
                            <h3>Fan Box</h3>
                            <ul>

                                <a href="/loja/clic.php?loja=352023&banner=171" target="_parent"> 
                                    <img src='https://static3.tcdn.com.br/img/img_prod/352023/1527105578_lojadc_extra1_.jpg' title='' alt=''>

                                </a> 
                            </ul>
                        </li>
                        <li class="item-level1 cat-478">
                            <h3>
                                <a class="link-level1" href="https://www.lojadccomics.com.br/personagens">Personagens</a>
                            </h3>
                            <ul class="level2">
                                <img src="https://static3.tcdn.com.br/img/img_prod/352023/categoria_img_478_20150831111106.jpg" alt="Personagens">
                                <div>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/personagens/comics" class="link-level2">Comics</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/super-herois/batman" class="link-level3">Batman</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/super-herois/superman" class="link-level3">Superman</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/super-herois/the-flash" class="link-level3">The Flash</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/comics/wonder-woman" class="link-level3">Wonder Woman</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/super-herois/lanterna-verde" class="link-level3">Lanterna Verde</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/super-herois/coringa" class="link-level3">The Joker</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/comics/harley-quinn" class="link-level3">Harley Quinn</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/universo-dc" class="link-level3">Universo DC Comics</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/comics/aquaman" class="link-level3">Aquaman</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/personagens/series-filmes-games" class="link-level2">Séries / Filmes / Games</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/liga-da-justica" class="link-level3">Liga da Justiça</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/wonder-woman-filme" class="link-level3">Wonder Woman</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/injustice-2" class="link-level3">Injustice 2</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/series-filmes-games/batman-vs-superman" class="link-level3">Batman VS Superman</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/series-filmes-games/esquadrao-suicida" class="link-level3">Esquadrão Suicida</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/series-filmes-games/batman-arkham-knight" class="link-level3">Batman Arkham Knight</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/series/the-flash" class="link-level3">The Flash</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/series/gotham" class="link-level3">Gotham</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/series/arrow" class="link-level3">Arrow</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/mad" class="link-level3">MAD</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/personagens/series-filmes-games/batman-a-piada-mortal" class="link-level3">Batman A Piada Mortal</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                </div>
                            </ul>
                        </li>
                        <li class="item-level1 cat-486">
                            <h3>
                                <a class="link-level1" href="https://www.lojadccomics.com.br/moda">Moda</a>
                            </h3>
                            <ul class="level2">
                                <img src="https://static3.tcdn.com.br/img/img_prod/352023/categoria_img_486_20180523182731.jpg" alt="Moda">
                                <div>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/camisetas-masculinas" class="link-level2">Roupas Masculinas</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/camisetas-masculinas/basica-unissex" class="link-level3">Camisetas</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/roupas-masculinas/moletons" class="link-level3">Moletons</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/roupas-masculinas/meias" class="link-level3">Meias</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/camisetas-femininas" class="link-level2">Roupas Femininas</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/camisetas-femininas/baby-look" class="link-level3">Camisetas</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/roupas-femininas/moletons" class="link-level3">Moletons</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/roupas-femininas/saias-e-shorts" class="link-level3">Saias e Shorts</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/roupas-femininas/meias" class="link-level3">Meias</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/infantil" class="link-level2">Roupas Infantis</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/infantil/camisetas" class="link-level3">Camisetas</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/infantil/bodies" class="link-level3">Bodies</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/roupas-infantis/meias" class="link-level3">Meias</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/infantil/pijamas" class="link-level3">Pijamas</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/acessorios" class="link-level2">Acessórios</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/acessorios/bijouterias" class="link-level3">Bijuterias</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/acessorios/bones" class="link-level3">Bonés e Gorros</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/acessorios/carteiras" class="link-level3">Carteiras</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/acessorios/fantasias" class="link-level3">Fantasias e Cosplay</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/mochilas" class="link-level3">Mochilas</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/acessorios/necessaires" class="link-level3">Necessaires</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/calcados" class="link-level2">Calçados</a>
                                        </h4>
                                        <ul class="level3">
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/calcados/chinelos" class="link-level3">Chinelos</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/produtos/calcados/tenis" class="link-level3">Tênis</a>
                                                </h5>
                                            </li>
                                            <li class="item-level3">
                                                <h5>
                                                    <a href="https://www.lojadccomics.com.br/moda/calcados/botas" class="link-level3">Botas</a>
                                                </h5>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/colecao-de-inverno" class="link-level2">Coleção de Inverno</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/moda/combos" class="link-level2">Combos</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/patins" class="link-level2">Patins</a>
                                        </h4>
                                    </li>
                                </div>
                            </ul>
                        </li>
                        <li class="item-level1 cat-30">
                            <h3>
                                <a class="link-level1" href="https://www.lojadccomics.com.br/produtos/casa-e-decoracao">Casa e Decoração</a>
                            </h3>
                            <ul class="level2">
                                <img src="https://static3.tcdn.com.br/img/img_prod/352023/categoria_img_30_20150831113715.jpg" alt="Casa e Decoração">
                                <div>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/almofadas" class="link-level2">Almofadas</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/moveis" class="link-level2">Móveis</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/organizadores" class="link-level2">Organizadores</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/canecas-e-copos" class="link-level2">Canecas</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/placas-decorativas" class="link-level2">Placas Decorativas</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/bonecos-colecionaveis" class="link-level2">Colecionáveis</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/quadros-e-posteres" class="link-level2">Quadros</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/copos" class="link-level2">Copos</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/radio-relogios-despertadores" class="link-level2">Relógios e Despertadores</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/cozinha" class="link-level2">Cozinha</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/squeezes" class="link-level2">Squeezes e Garrafas</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/quebra-cabecas" class="link-level2">Quebra-Cabeças</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/casa-e-decoracao/luminarias" class="link-level2">Luminárias</a>
                                        </h4>
                                    </li>
                                </div>
                            </ul>
                        </li>
                        <li class="item-level1 cat-494">
                            <h3>
                                <a class="link-level1" href="https://www.lojadccomics.com.br/acessorios">Acessórios</a>
                            </h3>
                            <ul class="level2">
                                <img src="https://static3.tcdn.com.br/img/img_prod/352023/categoria_img_494_20150831114106.jpg" alt="Acessórios">
                                <div>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/audio-e-gadgets" class="link-level2">Áudio e Gadgets</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/button" class="link-level2">Buttons</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/informatica-e-acessorios" class="link-level2">Informática e Acessórios</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/uso-pessoal" class="link-level2">Uso Pessoal</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/chaveiros" class="link-level2">Chaveiros</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/patins" class="link-level2">Patins</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/prancha-de-surfe" class="link-level2">Prancha de Surfe</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/skates" class="link-level2">Skates</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/acessorios/capacetes" class="link-level2">Capacetes</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/cosmeticos" class="link-level2">Cosméticos</a>
                                        </h4>
                                    </li>
                                </div>
                            </ul>
                        </li>
                        <li class="item-level1 cat-496">
                            <h3>
                                <a class="link-level1" href="https://www.lojadccomics.com.br/filmes-tvs-games">Filmes / TVs / Games</a>
                            </h3>
                            <ul class="level2">
                                <img src="https://static3.tcdn.com.br/img/img_prod/352023/categoria_img_496_20150831115349.jpg" alt="Filmes / TVs / Games">
                                <div>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/dvds-e-blu-ray" class="link-level2">DVDs e Blu-Ray</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/games" class="link-level2">Games</a>
                                        </h4>
                                    </li>
                                </div>
                            </ul>
                        </li>
                        <li class="item-level1 cat-498">
                            <h3>
                                <a class="link-level1" href="https://www.lojadccomics.com.br/livraria-papelaria">Livraria / Papelaria</a>
                            </h3>
                            <ul class="level2">
                                <img src="https://static3.tcdn.com.br/img/img_prod/352023/categoria_img_498_20161209170922.jpg" alt="Livraria / Papelaria">
                                <div>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/livraria-papelaria/mochilas" class="link-level2">Mochilas</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/papelaria" class="link-level2">Cadernos</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/produtos/graphic-novel-e-hq" class="link-level2">Graphic Novel e HQ</a>
                                        </h4>
                                    </li>
                                    <li class="item-level2">
                                        <h4>
                                            <a href="https://www.lojadccomics.com.br/livraria-papelaria/blocos-de-anotacoes" class="link-level2">Blocos de Anotações</a>
                                        </h4>
                                    </li>
                                </div>
                            </ul>
                        </li>
                    </ul>        </div>
            </nav>
            <!-- /category-list -->
        </header>

        <!--mobile responsive-->
        <div id="topo-mob">
            <div class="logo-mob">
                <a class="brand" href="/" title="Loja Virtual">                    
                    <img class="img-circle imagem-logo" src="<?= base_url() ?>dist/img/lojavirtual.jpg" alt="imagem de loja virtual" alt="Loja Virtual" />
                </a>
            </div>
            <div class="menutopo-mob">
                <ul>
                    <li class="li-mob trigger-menu" id="menu-categorias-mob">
                        <a class="a-mob" id="menu-toggle-mob">
                            <svg class="icon icon-menu51" viewBox="0 0 32 32">
                            <path class="path1" d="M30.966 0.603h-29.932c-0.57 0-1.034 0.464-1.034 1.034v4.596c0 0.57 0.464 1.034 1.034 1.034h29.932c0.57 0 1.034-0.464 1.034-1.034v-4.596c0-0.57-0.464-1.034-1.034-1.034z"></path>
                            <path class="path2" d="M30.966 12.668h-29.932c-0.57 0-1.034 0.464-1.034 1.034v4.596c0 0.57 0.464 1.034 1.034 1.034h29.932c0.57 0 1.034-0.464 1.034-1.034v-4.596c0-0.57-0.464-1.034-1.034-1.034z"></path>
                            <path class="path3" d="M30.966 24.733h-29.932c-0.57 0-1.034 0.464-1.034 1.034v4.596c0 0.57 0.464 1.034 1.034 1.034h29.932c0.57 0 1.034-0.464 1.034-1.034v-4.596c0-0.57-0.464-1.034-1.034-1.034z"></path>
                            </svg>
                            <div class="nameMenu">Menu</div>
                        </a>
                    </li>
                    <li class="li-mob">
                        <a class="a-mob" href="/central-do-cliente">
                            <svg class="icon icon-user-mobile" viewBox="0 0 32 32">
                            <path class="path1" d="M16 0c-8.852 0-16 7.17-16 16 0 8.84 7.161 16 16 16 8.879 0 16-7.194 16-16 0-8.851-7.175-16-16-16zM24.234 25.424c-2.281 1.999-5.173 3.091-8.234 3.091s-5.952-1.092-8.232-3.089c-0.309-0.27-0.448-0.686-0.366-1.087 0.788-3.848 3.239-6.883 6.359-7.881-1.628-0.964-2.746-2.944-2.746-5.232 0-3.234 2.232-5.856 4.985-5.856s4.985 2.622 4.985 5.856c0 2.288-1.117 4.267-2.745 5.231 3.12 0.998 5.571 4.033 6.359 7.881 0.082 0.401-0.058 0.817-0.366 1.087z"></path>
                            </svg>
                            <div class="nameMenu">Entrar</div>
                        </a>
                    </li>
                    <li class="li-mob">
                        <a class="a-mob" href="/loja/carrinho.php?loja=352023">
                            <svg class="icon icon-shopping-cart" viewBox="0 0 32 32">
                            <path class="path1" d="M13.594 27.555c0 1.502-1.217 2.719-2.719 2.719s-2.719-1.217-2.719-2.719c0-1.502 1.217-2.719 2.719-2.719s2.719 1.217 2.719 2.719z"></path>
                            <path class="path2" d="M24.469 27.555c0 1.502-1.217 2.719-2.719 2.719s-2.719-1.217-2.719-2.719c0-1.502 1.217-2.719 2.719-2.719s2.719 1.217 2.719 2.719z"></path>
                            <path class="path3" d="M0 1.726h5.438v2.719h-5.438v-2.719z"></path>
                            <path class="path4" d="M7.585 7.164l-0.788-5.438h-1.359l-1.359 0.19 2.896 20.2h18.379l6.647-14.953h-24.415zM23.585 19.398h-14.246l-1.359-9.516h19.833l-4.228 9.516z"></path>
                            </svg>
                            <span class="contadorcarrinho" data-cart="amount">0</span>
                            <div class="nameMenu">Carrinho</div>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="clearboth searchmobile">
                <form method="get" action="/loja/busca.php?loja=352023" class="search" name="buscaForm" data-search="suggestion">
                    <input type="hidden" name="loja" value="352023" />
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="input-group mm-input-group">                                
                                <input class="form-control search-key" data-input="suggestion" placeholder="Procurar na loja" tabindex="0" type="text" name="palavra_busca" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default glyphicon glyphicon-search search-button" type="submit"></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form><!-- .search -->
            </div>

        </div>
        <!--fim mobile responsive-->


